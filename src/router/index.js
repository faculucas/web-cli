import Vue from 'vue'
import Router from 'vue-router'

//  Views
import HelloWorldView from '@/views/HelloWorld'
import HelloView from '@/views/Hello'
import ProductosView from '@/views/Productos'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorldView
    },
    {
      path: '/hello',
      name: 'Hello',
      component: HelloView
    },
    {
      path: '/productos',
      name: 'Productos',
      component: ProductosView
    }
  ]
})
